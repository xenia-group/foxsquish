FROM docker:latest

RUN apk add nodejs npm rsync openssh-client && apk add --repository=https://dl-cdn.alpinelinux.org/alpine/edge/community squashfs-tools-ng

RUN mkdir /app

WORKDIR /app

COPY package*.json ./
COPY patches ./patches

RUN npm install

COPY . .

RUN npm run build

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["npm", "run", "start"]
