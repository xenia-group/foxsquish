/**
 * Critical error; program quits with code 1
 *
 * @param {string} name to be printed
 * @param {unknown} e error object
 */

export const die = (name: string, e: unknown, cleanup?: Function): never => {
  console.error('[CRITICAL] ' + name + ' failed!');
  console.error(e);
  console.trace()
  if (cleanup) {
    cleanup();
  }
  process.exit(1);
};
