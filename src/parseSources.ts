import { existsSync, readFileSync } from 'fs';
import { config } from './config.js';
import { validateRegistryLink } from './validateRegistryLink.js';
import { die } from './die.js';
import { parseSourcesFromArgv, hasArgvSources } from './parseSourcesFromArgv.js';
import { log } from './helpers.js';

/**
 * Parses the image sources.
 * expects \n separated textfile or JSON top-level array ('["a","b"]')
 * @returns {Array<string>}
 */

export default (): string[] => {
  let file: string = '';

  if (config.PREFER_SOURCE_FROM_ARGV && hasArgvSources) {
    log(parseSourcesFromArgv());
    return parseSourcesFromArgv();
  }

  // single file defined
  if (typeof config.SOURCES_FILE === 'string') {
    if (existsSync(config.SOURCES_FILE)) {
      file = config.SOURCES_FILE;
    } else {
      // no file
      if (hasArgvSources) {
        return parseSourcesFromArgv();
      }
      die('parse sources', new Error("The file config.SOURCES_FILES doesn't exist:\n" + config.SOURCES_FILE));
    }
  } else {
    // array of files
    file = ((): string => {
      for (const file of config.SOURCES_FILE) {
        if (existsSync(file)) {
          log('source: ', file);
          return file;
        }
      }
      // no file:
      if (hasArgvSources) {
        return '__GET_FROM_ARGV__'; // NOTE: bad
      }
      die(
        'parse sources',
        new Error('None of the files in config.SOURCES_FILES exist:\n' + config.SOURCES_FILE)
      );
      return ''; // unreachable
    })();
  }

  if (file === '__GET_FROM_ARGV__') {
    return parseSourcesFromArgv();
  }

  // read file
  let content: string = '';
  let parsed: string[] = [];
  try {
    content = readFileSync(file, { encoding: 'utf8' });
  } catch (e) {
    die('parse sources', e);
  }

  // parse
  try {
    // JSON
    parsed = JSON.parse(content);
    if (!Array.isArray(parsed)) {
      die('parse sources', new Error("JSON source file isn't an array"));
    } else {
      parsed.map((line) => {
        return line.trim();
      });
    }
  } catch (e) {
    // \n separated
    parsed = content.split('\n');
    parsed.map((line) => {
      return line.trim();
    });
  }
  parsed = parsed.map((e) => e.replace(/#.*/, '')); // removes hash prefixed comment
  parsed = parsed.filter((e) => e); // removes empty
  for (const url of parsed) {
    if (!validateRegistryLink(url)) {
      die('parse sources', new Error('Invalid url: ' + url));
    }
  }
  return parsed;
};
