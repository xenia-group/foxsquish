import { validateRegistryLink } from './validateRegistryLink.js';

export const parseSourcesFromArgv = (): string[] => {
  // poundland argv parser
  const argvSources: string[] = [];

  const argvSourcesArr = process.argv.filter((e) => /^SOURCE(S)?=/.test(e));

  argvSourcesArr.forEach((e) => {
    let clean = e.replace(/^SOURCE(S)?=/, '');
    clean = clean.replace(/\"/g, '');
    const split = clean.split(',');
    split.forEach((a) => validateRegistryLink(a));
    split.forEach((b) => argvSources.push(b));
  });

  return argvSources;
};

export const hasArgvSources = parseSourcesFromArgv().length !== 0;
