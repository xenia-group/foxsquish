import { config } from './config.js';
import { die } from './die.js';
import shell from 'shelljs';
import { sourceToPath, ensureDir, logv } from './helpers.js';
import { rmSync } from 'node:fs';
import docker from './docker.js';

export const squish = (
  source: string,
  removeArchiveAfterSquish: boolean = config.REMOVE_ARCHIVE_AFTER_SQUISH,
  removeDockerImageAfterSquish: boolean = config.REMOVE_DOCKER_IMAGE_AFTER_SQUISH
): boolean => {
  const tarPath = sourceToPath(source, config.WORK_DIR);
  const imgPath = sourceToPath(source, config.WORK_DIR, true, false);
  const bin = config.TAR2SQFS_BINARY;

  ensureDir(sourceToPath(source, config.WORK_DIR, true, true));

  const out = shell.exec(`cat "${tarPath}" | ${bin} -q "${imgPath}"`);
  if (out.code !== 0) {
    die('squish', new Error(out.stderr));
    return false;
  }
  if (removeDockerImageAfterSquish) {
    if (config.REMOVE_DOCKER_IMAGE_AFTER_EXPORT) {
        logv('warn: docker image already removed since REMOVE_DOCKER_IMAGE_AFTER_EXPORT is set.');
    } else {
      docker.removeImage(source);
    }
  }
  if (removeArchiveAfterSquish) {
    removeArchive(source);
  }
  return true;
};

const removeArchive = (source: string): boolean => {
  try {
    rmSync(sourceToPath(source, config.WORK_DIR));
  } catch (e) {
    console.error(`! failed removing ${sourceToPath(source, config.WORK_DIR)}`);
    console.error(e);
    return false;
    // NOTE: should this be critical?
  }
  return true;
};
