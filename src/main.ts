import { log, logv } from './helpers.js';
import * as squishJs from './squish.js';
import prepareDirectories from './prepareDirectories.js';
import docker from './docker.js';
import parseSources from './parseSources.js';
import { config } from './config.js';
import { checkDeps } from './checkDeps.js';
import { checkSpace } from './checkSpace.js';
import { die } from './die.js';
import { generateManifest } from './generateManifest.js';

const main = async () => {
  logv(config);

  checkDeps();

  prepareDirectories();
  const sources = parseSources();

  if (sources.length === 0) {
    die('get sources', new Error('no sources found'));
  }

  log(`Found ${sources.length} image sources`);

  await checkSpace(sources);

  if (config.GENERATE_MANIFEST) {
    await generateManifest(sources, config.MANIFEST_SOURCE);
  }

  if (config.PULL_ALL_FIRST) {
    let i: number = 0;
    let source_count: number = sources.length;
    log('fetching images');
    for (const source of sources) {
      i++;
      log(`[${i}/${source_count}] fetching ${source}`);
      docker.pull(source);
    }
  }

  let i: number = 0;
  let source_count: number = sources.length;
  for (const source of sources) {
    i++;
    if (!config.PULL_ALL_FIRST) {
      log('fetching image');
      docker.pull(source);
    }

    log(`[${i}/${source_count}] Processing ${source}`);

    log('creating container');
    docker.create(source);

    log('exporting container');
    docker.export(source);

    log('removing container');
    docker.remove(source);

    if (config.SQUISH_IMMEDIATELY) {
      log('squishing the tar into a cute little foxie');
      squishJs.squish(source);
    }
  }
  if (!config.SQUISH_IMMEDIATELY) {
    let i: number = 0;
    let source_count: number = sources.length;
    for (const source of sources) {
      log(`[${i}/${source_count}] squishing ${source}`);
      squishJs.squish(source);
    }
  }
  log('All done!');
  logv('No animals were harmed during the generation of the image.');
};

main();
