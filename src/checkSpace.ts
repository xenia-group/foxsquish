import { config } from './config.js';
import { die } from './die.js';
import checkDiskSpace from 'check-disk-space';
import { log, logv } from './helpers.js';

export const checkSpace = async (sources: string[]): Promise<boolean> => {
  /* NOTE: so this is what's needed to keep in mind:
 
  archive size and image size are checked for in the work directory
  docker image size is checked in /var/lib/docker.
  both configurable
  
  if SQUISH_IMMEDIATELY and REMOVE_ARCHIVE_AFTER_SQUISH aren't set, the archives stay
  on the system up until the end => more space;
 
  REMOVE_ARCHIVE_AFTER_SQUISH alone not being set also
  makes it take up more space; since the archives stay.
 
  PULL_ALL_FIRST *being set* causes all images to exist at the same time,
  it will eventually start freeing up space if REMOVE_DOCKER_IMAGE_* is set
  but the calculation is already getting quite complicated so ignoring that.
 
  WARN: docker being docker, /var/lib/docker/overlay2 gets polluted;
  docker system prune -a can help, but be very careful;
  as it could mess with your own containers; DO YOUR RESEARCH;
  TODO: test if REMOVE_DOCKER_IMAGE_AFTER_EXPORT has any
  beneficial effects;
  best solution currently, if you use docker for anything else
  use a vm; keep an image vm before using foxsquish and reset when needed.
  assume system + 30GB per image.;
 
  NOTE: if anyone got an idea how to cleanup better safely, lmk;
 
  if xenia ever gets larger, change ASSUMED_{ARCHIVE,IMAGE}_SIZE;
  currently there is a little bit of a reserve, i think.;
 
  TODO: include in docs
  TODO: write docs
 
  And finally, feel free to rewrite all of this, it's a mess.
 
  */

  const need = getSize(sources);
  const free = await getFree();

  log(`checking for ${need.work}GB in ${config.WORK_DIR}`);
  log(`checking for ${need.docker}GB in ${config.DOCKER_IMAGE_DIR}`);

    logv(`Dir mountpoints: ${free.work.diskPath}, ${free.docker.diskPath}`);
    logv(`Free: ${BtoGB(free.work.free)}GB, ${BtoGB(free.docker.free)}GB`);
    logv(`Needed: ${need.work}GB, ${need.docker}GB`);

  if (free.work.diskPath === free.docker.diskPath) {
    logv('Same mountpoint; combined size');
    if (need.work + need.docker > BtoGB(free.docker.free) + BtoGB(free.work.free)) {
      console.error(`Not enough space on ${free.work.diskPath}, need ${need.work + need.docker}GB`);
      if (config.IGNORE_SPACE_CHECK) {
        log('Ignoring failed space check');
        return true;
      } else {
        die(
          'space check',
          new Error(`Not enough space on ${free.work.diskPath} (${need.work + need.docker}GB)`)
        );
        return false;
      }
    }
  } else {
    let failed: boolean = false;
    if (need.work > BtoGB(free.work.free)) {
      console.error(`Not enough space on ${free.work.diskPath}, need ${need.work}`);
      failed = true;
    }
    if (need.docker > BtoGB(free.docker.free)) {
      console.error(`Not enough space on ${free.docker.diskPath}, need ${need.docker}`);
      failed = true;
    }
    if (failed && config.IGNORE_SPACE_CHECK) {
      log('continuing');
      return true;
    } else if (failed) {
      die('space check', new Error('not enough space'));
    }
    return true;
  }

  return true;
};

const getSize = (sources: string[]) => {
  const COUNT = sources.length;

  const oneArchiveAtATime = config.REMOVE_ARCHIVE_AFTER_SQUISH && config.SQUISH_IMMEDIATELY;

  const oneDockerImageAtATime = ((): boolean => {
    if (config.PULL_ALL_FIRST) {
      return false;
    }
    return config.REMOVE_DOCKER_IMAGE_AFTER_EXPORT || config.REMOVE_DOCKER_IMAGE_AFTER_SQUISH;
  })();

  const _Archives = config.ASSUMED_ARCHIVE_SIZE * (oneArchiveAtATime ? 1 : COUNT);

  const _Images = COUNT * config.ASSUMED_IMAGE_SIZE;

  const _DockerImages = config.ASSUMED_DOCKER_IMAGE_SIZE * (oneDockerImageAtATime ? 1 : COUNT);

  const work = _Images + _Archives;
  const docker = config.IGNORE_DOCKER_IMAGE_SPACE_CHECK ? 0 : _DockerImages;

  return {
    work: work,
    docker: docker,
  };
};

const BtoGB = (num: number): number => {
  return num / Math.pow(1000, 3);
};

const getFree = async () => {
  try {
    // @ts-ignore
    var work = await checkDiskSpace(config.WORK_DIR);
    // @ts-ignore
    var docker = await checkDiskSpace(config.DOCKER_IMAGE_DIR);
  } catch (e) {
    if (config.IGNORE_SPACE_CHECK) {
      console.error('space check failed:');
      console.error(e);
      console.error('ignoring space check error');
    } else {
      die('space check', e);
    }
  }
  return { work: work, docker: docker };
};
