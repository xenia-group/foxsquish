import { execSync } from 'child_process';
import * as path from 'node:path/posix';
import { config } from './config.js';
import { die } from './die.js';

/**
 * Creates work directories
 * (Critical)
 * @returns {boolean}
 */

export default (): boolean => {
  [path.join(config.WORK_DIR, 'tmp'), path.join(config.WORK_DIR, 'build')].forEach((path) => {
    try {
      execSync(`mkdir -p "${path}"`);
    } catch (e) {
      die('prepare directories', e);
    }
  });
  return true;
};
