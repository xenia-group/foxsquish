import * as toml from 'smol-toml';
import { objMerge, getArch, log, logv, sourceToInfo, sourceToName } from './helpers.js';
import { die } from './die.js';
import { writeFileSync } from 'fs';
import { config } from './config.js';

export const generateManifest = async (
  sources: string[],
  baseManifestUrl: string | URL | null = null
): Promise<boolean> => {
  log("generating manifest")
  var manifest: Record<string, any> = {};
  if (baseManifestUrl !== null) {
    logv('Using existing online manifest');
    logv('Manifest source: ', baseManifestUrl);
    let res;
    try {
      logv('Fetching existing manifest');
      res = await fetch(baseManifestUrl);
    } catch (e) {
      die('fetch manifest', e);
      return false;
    }
    try {
      logv('Parsing existing manifest');
      const baseToml = await res.text();
      manifest = toml.parse(baseToml);
    } catch (e) {
      die('parse manifest', e);
      return false;
    }
  }

  // this is what it should look like:
  // const test = {
  //   'gnome-systemd': {
  //     arch: ['arm'],
  //     name: 'gnome-systemd',
  //     versions: {
  //       unstable: {
  //         arch: ['arm'],
  //         filename: 'gnome-systemd.img',
  //       },
  //     },
  //   },
  // };

  let newManifest: Record<string, any> = {};
  for (const source of sources) {
    const name = sourceToInfo(source, 'name');
    const branch = sourceToInfo(source, 'branch');
    const filename = sourceToInfo(source, 'filename');
    const manifestElement: Record<string, any> = {};
    manifestElement[name] = {
      arch: [getArch()],
      name: name,
      versions: {},
    };

    manifestElement[name].versions[branch] = {
      arch: [getArch()],
      filename: filename,
    };

    // console.log(manifest)
    manifest = objMerge(manifest, manifestElement);
  }

  try {
    logv('writing manifest to ', config.MANIFEST_OUTPUT);
    writeFileSync(config.MANIFEST_OUTPUT, toml.stringify(manifest));
  } catch (e) {
    die('write manifest', e);
  }
  logv('manifest done');
  return true;
};
