import { config } from './config.js';
import { die } from './die.js';
import which from 'which';
import { log } from './helpers.js';

export const checkDeps = (): boolean => {
  try {
    which.sync(config.DOCKER_BINARY);
    which.sync(config.TAR2SQFS_BINARY);
  } catch (e) {
    console.error('! make sure you have docker and squashfs-tools-ng.');
    die('check for dependencies', e);
  }
  log(`using ${config.DOCKER_BINARY} & ${config.TAR2SQFS_BINARY}`);
  return true;
};
