import { spawnSync } from 'child_process';
import * as path from 'node:path/posix';
import { config } from './config.js';
import { die } from './die.js';

export const sourceToName = (
  url: string,
  include_prefix: boolean = false,
): string => {
  const split = url.split('/');
  let str: string = '';
  if (include_prefix) {
    str += 'foxsquish_';
  }
    str += split[split.length - 1].replace(':', config.IMAGE_TAG_SEPARATOR); // gnome-systemd___main
  return str; //gnome-systemd___main || foxsquish_gnome-systemd___main
};

export const sourceToInfo = (
  url: string,
  info: 'name' | 'branch' | 'major' | 'minor' | 'filename'
): string => {
  const image = url.split('/')[url.split('/').length - 1];

  switch (info) {
    case 'name':
      return image.split(':')[0];
    case 'branch':
      return image.split(':')[1];
    case 'major':
      return image.split(':')[0].split('-')[0];
    case 'minor':
      return image.split(':')[0].split('-')[1];
    case 'filename':
      return image.split(':')[0] + '.img';
  }
};

export const sourceToPath = (
  source: string,
  workPath: string,
  squashed: boolean = false,
  onlyDirectory: boolean = false
) => {
  // work/(tmp||build)/arch/tag/image.(tar||img)
  let str: string = '';
  str += squashed ? 'build/' : 'tmp/';
  // arch
  str += getArch();
  str += '/';
  // tag
  str += (() => {
    const s = source.split(':');
    return s[s.length - 1];
  })();
  str += '/';

  if (!onlyDirectory) {
    // image name
    str += (() => {
      const a = source.split('/');
      return a[a.length - 1].split(':')[0];
    })();
    // extention
    str += squashed ? '.img' : '.tar'; // if this changes, edit sourceToInfo()
  }
  // abs path
  return path.join(workPath, str);
};

export const ensureDir = (dir: string) => {
  try {
    const out = spawnSync('mkdir', ['-p', dir]);
    if (out.status != 0) {
      die('ensure directory', new Error(out.output.toString()));
    }
  } catch (e) {
    die('ensure directory', e);
  }
};

/**
 * Merge objects, remove duplicate array entries
 * https://gist.github.com/ahtcx/0cd94e62691f539160b32ecda18af3d6?permalink_comment_id=3571894#gistcomment-3571894
 * please don't steal this it's not good
 *
 * @param {Record<string, any>} target
 * @param {Record<string, any>} source
 * @param {boolean} [isMergingArrays=false]
 * @returns {Record<string, any>}
 */
export const objMerge = (
  target: Record<string, any>,
  source: Record<string, any>,
  isMergingArrays = false
): Record<string, any> => {
  const isObject = (obj: any) => obj && typeof obj === 'object';

  if (!isObject(target) || !isObject(source)) return source;

  Object.keys(source).forEach((key: string) => {
    const targetValue = target[key];
    const sourceValue = source[key];

    if (Array.isArray(targetValue) && Array.isArray(sourceValue))
      if (isMergingArrays) {
        target[key] = targetValue.map((x, i) =>
          sourceValue.length <= i ? x : objMerge(x, sourceValue[i], isMergingArrays)
        );
        if (sourceValue.length > targetValue.length)
          target[key] = [...new Set(target[key].concat(sourceValue.slice(targetValue.length)))];
      } else {
        target[key] = [...new Set(targetValue.concat(sourceValue))];
      }
    else if (isObject(targetValue) && isObject(sourceValue))
      target[key] = objMerge(Object.assign({}, targetValue), sourceValue, isMergingArrays);
    else target[key] = sourceValue;
  });

  return target;
};

export const getArch = (): string => {
  const archList = {
    x64: 'amd64',
    arm: 'arm',
    arm64: 'arm64',
    mips: 'mips',
    mipsel: 'mips',
    loong64: 'loong64',
    ppc: 'ppc',
    ppc64: 'ppc64',
    riscv64: 'riscv64',
    ia32: 'ita',
    s390: 'ibm390',
    s390x: 'ibmz',
  };
  return archList[process.arch];
};

/**
 * Log message if verbose is enabled
 * Otherwise identical to console.log
 * @param {?*} [message]
 * @param {...any[]} optionalParams
 */
export const logv = (message?: any, ...optionalParams: any[]): void => {
  if (config.VERBOSE) {
    console.log('[VERBOSE] ' + message, ...optionalParams);
  }
};

/**
 * Log message
 * @param {?*} [message]
 * @param {...any[]} optionalParams
 */
export const log = (message?: any, ...optionalParams: any[]): void => {
  console.log('[INFO] ' + message, ...optionalParams);
};
