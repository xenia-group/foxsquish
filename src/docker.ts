import { spawnSync } from 'child_process';
import { config } from './config.js';
import { sourceToName, ensureDir, sourceToPath, log } from './helpers.js';
import { die } from './die.js';

const docker = {
  bin: config.DOCKER_BINARY,
  pull: (source: string) => {
    try {
      // NOTE: show progress?
      const out = spawnSync(docker.bin, ['pull', source]);
      if (out.status != 0) {
        die('pull image', new Error('why no stderr ugh'));
      }
      log(`got ${source}`);
    } catch (e) {
      die('pull image', e);
    }
  },
  create: (source: string): boolean => {
    try {
      const out = spawnSync(docker.bin, ['create', '--name', sourceToName(source, true), source]);
      if (out.status !== 0) {
        die('create container', out.stderr.toString());
        return false;
      }
    } catch (e) {}
    return true;
  },
  remove: (source: string): boolean => {
    try {
      const out = spawnSync(docker.bin, ['rm', sourceToName(source, true)]);
      if (out.status !== 0) {
        die('remove container', out.stderr.toString());
        return false;
      }
    } catch (e) {}
    return true;
  },
  removeImage: (source: string): boolean => {
    try {
      const out = spawnSync(docker.bin, ['image', 'rm', sourceToName(source, true)]);
      if (out.status !== 0) {
        die('remove image', out.stderr.toString());
        return false;
      }
    } catch (e) {}
    return true;
  },
  //docker export -o docker-export-test.tar xenia-test
  export: (source: string): boolean => {
    try {
      ensureDir(sourceToPath(source, config.WORK_DIR, false, true));
      const out = spawnSync(docker.bin, [
        'export',
        '-o',
        sourceToPath(source, config.WORK_DIR),
        sourceToName(source, true),
      ]);
      if (out.status !== 0) {
        die('export container', out.stderr.toString(), () => {
          log('removing container');
          docker.remove(source);
        });
        return false;
      }
    } catch (e) {}
    if (config.REMOVE_DOCKER_IMAGE_AFTER_EXPORT) {
      // NOTE: image does not get removed if export fails
      docker.removeImage(source);
    }
    return true;
  },
};

export default docker;
