import * as path from 'node:path/posix';
import { fileURLToPath } from 'node:url';

const cwd: string = path.normalize(path.join(path.dirname(fileURLToPath(import.meta.url)), '..'));

/**
 * Generate absolute path to base directory
 *
 * @param {string} _path subfolder/file
 * @returns {string}
 */
const pBase = (_path: string): string => {
  return path.normalize(path.join(cwd, _path));
};

interface Config {
  VERBOSE: boolean;
  DOCKER_BINARY: string;
  TAR2SQFS_BINARY: string;
  CWD: string;
  WORK_DIR: string;
  DOCKER_IMAGE_DIR: string;
  PREFER_SOURCE_FROM_ARGV: boolean;
  SOURCES_FILE: Array<string> | string;
  GENERATE_MANIFEST: boolean;
  MANIFEST_SOURCE: string | null;
  MANIFEST_OUTPUT: string;
  ASSUMED_DOCKER_IMAGE_SIZE: number;
  ASSUMED_ARCHIVE_SIZE: number;
  ASSUMED_IMAGE_SIZE: number;
  IGNORE_SPACE_CHECK: boolean;
  IGNORE_DOCKER_IMAGE_SPACE_CHECK: boolean;
  REMOVE_DOCKER_IMAGE_AFTER_EXPORT: boolean;
  REMOVE_DOCKER_IMAGE_AFTER_SQUISH: boolean;
  REMOVE_ARCHIVE_AFTER_SQUISH: boolean;
  PULL_ALL_FIRST: boolean;
  SQUISH_IMMEDIATELY: boolean;
  IMAGE_TAG_SEPARATOR: string;
  BUILD_CONTAINER_PREFIX: string;
}

/* CONFIG */

export const config: Config = {
  VERBOSE: true,
  DOCKER_BINARY: 'docker',
  TAR2SQFS_BINARY: 'tar2sqfs',
  CWD: cwd, // base directory
  WORK_DIR: pBase('work'), // output directory
  PREFER_SOURCE_FROM_ARGV: true,
  SOURCES_FILE: [pBase('sources.json'), pBase('sources.txt'), pBase('sources')], // checked in order, LTR
  GENERATE_MANIFEST: true,
  MANIFEST_SOURCE: 'https://repo.xenialinux.com/releases/Manifest.toml',
  MANIFEST_OUTPUT: pBase('work/build/Manifest.toml'),
  // in GB; used for space requirement check estimation:
  ASSUMED_DOCKER_IMAGE_SIZE: 14,
  ASSUMED_ARCHIVE_SIZE: 10,
  ASSUMED_IMAGE_SIZE: 5,
  IGNORE_SPACE_CHECK: false,
  IGNORE_DOCKER_IMAGE_SPACE_CHECK: true,
  DOCKER_IMAGE_DIR: '/var/lib/docker/overlay2', // docker default, dont touch unless you changed docker config

  REMOVE_DOCKER_IMAGE_AFTER_EXPORT: false, // preceeds squish
  REMOVE_DOCKER_IMAGE_AFTER_SQUISH: false,
  REMOVE_ARCHIVE_AFTER_SQUISH: true,
  PULL_ALL_FIRST: true,
  SQUISH_IMMEDIATELY: true,
  IMAGE_TAG_SEPARATOR: '___', // most likely dont touch thnk u
  BUILD_CONTAINER_PREFIX: 'foxsquish_', // shouldnt need to touch this
};
/* */
